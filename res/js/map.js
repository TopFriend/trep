//var myCenter=new google.maps.LatLng(51.508742,-0.120850);

function placeMarker(position, map) {
    var marker = new google.maps.Marker({
        position: position,
        map: map,
        icon: 'res/img/logo2.png'
    });
    map.panTo(position);
}
function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
    directionsService.route({
        origin: pointA,
        destination: pointB,
        avoidTolls: true,
        durationInTraffic: true,
        optimizeWaypoints: true,
        provideRouteAlternatives: true,
        avoidHighways: false,
        avoidTolls: false,
        avoidHighways: false,
        travelMode: google.maps.TravelMode.WALKING
    }, function (response, status) {

        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            computeTotalDistance(directionsDisplay.directions);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

function computeTotalDistance(result) {
    var total = 0;
    var time = 0;
    var from = 0;
    var to = 0;
    var myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
        total += myroute.legs[i].distance.value;
        time += myroute.legs[i].duration.text;
        from = myroute.legs[i].start_address;
        to = myroute.legs[i].end_address;


    }
    time = time.replace('hours', 'H');
    time = time.replace('mins', 'M');
    total = total / 1000.
    alert(from)
    alert(to)
    alert(time)
    alert(Math.round(total) + "KM")

    //document.getElementById('from').innerHTML = from + '-'+to;
    //document.getElementById('duration').innerHTML = time ;
    //document.getElementById('total').innerHTML =Math.round( total)+"KM" ;

    var trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(map);

}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    Curlat = position.coords.latitude;
    Curlng = position.coords.longitude;
    pointC = new google.maps.LatLng(Curlat, Curlng);
    //alert("lat:" + Curlat + " lng:" + Curlng);
    myOptions = {
        zoom: 15,
        center: pointC,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('googleMap'), myOptions);
    google.maps.event.addDomListener(window, 'load', initialize);
    google.maps.event.addDomListener(window, "resize", function () {
        google.maps.event.trigger(map, "resize");
        map.setCenter(pointC);
    });
    directionsService = new google.maps.DirectionsService,
        directionsDisplay = new google.maps.DirectionsRenderer({
            map: map
        });
    markerC = new google.maps.Marker({
        position: pointC,
        title: "point C",
        label: "C",
        map: map
    });
    google.maps.event.addListener(map, 'click', function (e) {
        placeMarker(e.latLng, map);
        latLngB = e.latLng;
        pointD = new google.maps.LatLng(latLngB.lat(), latLngB.lng());

        calculateAndDisplayRoute(directionsService, directionsDisplay, pointC, pointD);
    });

}

function initialize() {
    getLocation();

}

google.maps.event.addDomListener(window, 'load', initialize);



