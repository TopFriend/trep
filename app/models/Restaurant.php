<?php

/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 9/11/2016
 * Time: 4:45 PM
 */
include 'BaseCrud.php';
class Restaurant extends BaseCrud
{
    const TBL_NAME ="tbl_restaurant";
    const TBL_PK_ID = "ID";
    public $ID;
    public $rest_name;
    public $rest_address;
    public $rest_phone;
    public $rest_email;
    public $rest_type;
    public $rest_description;
    public $rest_pass;
    public $enabled;


    function __construct()
    {
        parent::__construct();
    }
}