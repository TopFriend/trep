<?php

/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 7/3/2016
 * Time: 9:51 AM
 */
class BaseCrud extends CI_Model
{
    const TBL_NAME = "abstract";
    const TBL_PK_ID = "abstract";

    function __construct()
    {
        parent::__construct();

        $this->load->database();
    }

    public function insert()
    {
        $this->db->insert($this::TBL_NAME, $this);

    }

    private function update()
    {
        $this->db->update($this::TBL_NAME, $this, array($this::TBL_PK_ID => $this->{$this::TBL_PK_ID}));
    }

    public function save()
    {
        if (isset($this->{$this::TBL_PK_ID})) {
            $this->update();
        } else {
            $this->db->db_debug = false;
            $this->insert();
            $this->{$this::TBL_PK_ID} = $this->db->insert_id();

        }

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete()
    {
        $this->db->delete($this::TBL_NAME, array($this::TBL_PK_ID => $this->{$this::TBL_PK_ID}));
    }

    public function getById()
    {
        $query= $this->db->get_where($this::TBL_NAME, array($this::TBL_PK_ID => $this->{$this::TBL_PK_ID}));
        return $query->result();
    }



    public function getByExample($condition = array(), $offset = 20, $start = 0, $orderBy = "id", $asc = true)
    {
        if ($asc) {
            $asc = "asc";
        } else {
            $asc = "desc";
        }
        $this->db->select("*")
            ->from($this::TBL_NAME)
            ->where($condition)
            ->limit($offset, $start)
            ->order_by($orderBy, $asc);
        $query = $this->db->get();
       return $query->result();
    }

    public function populate($row)
    {
        foreach ($row as $key => $value) {
            $this->$key = $value;
        }
    }

    public function count_all()
    {
        return $this->db->count_all($this::TBL_NAME);
    }
}