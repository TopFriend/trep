<?php

/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 9/27/2016
 * Time: 4:57 PM
 */
class User extends BaseCrud
{
    const TBL_NAME ="tbl_users";
    const TBL_PK_ID = "ID";
    public $ID;
    public $FirstName;
    public $LastName;
    public $DateofBirth="y";
    public $SocialID="";
    public $Phone="";
    public $Email;
    public $Address="";
    public $City="ere";
    public $Picurl="";
    public $Describtion="";
    public $gender=0;
    public $age=0;
    public $pass;
    function __construct()
    {
        parent::__construct();
    }
}