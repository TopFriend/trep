<?php

/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 9/13/2016
 * Time: 2:09 PM
 */
class Other extends CI_Model
{
    private $tbl_name1 = "tbl_specification";
    private $tbl_name2 = "tbl_rest_specification";

    function __construct()
    {
        parent::__construct();

    }

    function get_all_spec()
    {
        $query = $this->db->get($this->tbl_name1);
        return $query->result();
    }

    function save_specs($data)
    {
        $this->db->insert($this->tbl_name2,$data);
    }
}