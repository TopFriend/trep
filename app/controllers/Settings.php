<?php

/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 9/19/2016
 * Time: 2:36 PM
 */
class Settings extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url", "form");
    }

    public function edit_pass()
    {
        if ($this->input->post("ajax") != null) {
            $data = array("a" => "d");
            echo $this->load->view("edit_pass", $data, true);
        } else {
            show_404();
        }
    }

    public function edit_rest_info()
    {
        if ($this->input->post("ajax")!=null)
        {
            $data = array("a"=>"b");
            echo $this->load->view("edit_rest_info",$data,true);
        }else
        {
            show_404();
        }
    }
}