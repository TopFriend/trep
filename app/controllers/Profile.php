<?php

/**
 * Created by IntelliJ IDEA.
 * User: ssalehi
 * Date: 6/13/2016
 * Time: 10:52 AM
 */
class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper("utils");
        $this->load->model("Restaurant");
    }

  function index($id=0)
  {
      if ($id!=0) {
          $rest = new Restaurant();
          $result = $rest->getByExample(array("enabled"=>true,"id"=>$id),0,0);
          $data["profile_info"]= $result[0];
          $this->load->view("profile",$data);
      }
  }

}