<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class HAuth extends CI_Controller
{

    public function __construct()
    {
        // Constructor to auto-load HybridAuthLib
        parent::__construct();
        $this->load->library('HybridAuthLib');
        $this->load->library('session');
        $this->load->helper('utils');
        $this->load->model('User');
    }

    public function index()
    {
        // Send to the view all permitted services as a user profile if authenticated
        $login_data['providers'] = $this->hybridauthlib->getProviders();
        foreach ($login_data['providers'] as $provider => $d) {
            if ($d['connected'] == 1) {
                $login_data['providers'][$provider]['user_profile'] = $this->hybridauthlib->authenticate($provider)->getUserProfile();
            }
        }

        echo $this->load->view('login', $login_data, true);
    }

    public function login($provider = "")
    {
        if ($provider != null) {
            log_message('debug', "controllers.HAuth.login($provider) called");
            try {
                log_message('debug', 'controllers.HAuth.login: loading HybridAuthLib');
                $this->load->library('HybridAuthLib');

                if ($this->hybridauthlib->providerEnabled($provider)) {
                    log_message('debug', "controllers.HAuth.login: service $provider enabled, trying to authenticate.");
                    $service = $this->hybridauthlib->authenticate($provider);

                    if ($service->isUserConnected()) {
                        log_message('debug', 'controller.HAuth.login: user authenticated.');
                        $user_profile = $service->getUserProfile();
                        $user = new User();
                        $user->ID = $user_profile->email;
                        $result = $user->getById();
                        if (sizeof($result)==0) {
                            $user->FirstName = $user_profile->firstName;
                            $user->LastName = $user_profile->lastName;
                            $user->gender = $user_profile->gender;
                            $user->age = $user_profile->age;
                            $user->Address = $user_profile->address!=null ? $user_profile->address : "";
                            $user->Email = $user_profile->email;
                            $user->Picurl = $user_profile->photoURL;
                            $user->Describtion = $user_profile->description;
                            $user->Phone = $user_profile->phone;
                            $pass = create_rand_pass();
                            $user->pass = sha1($pass);
                            $user->insert();
                            $data['pass'] =$pass ;
                            $data['user'] = $user->Email;
                            $message =  $this->load->view('confirm_mail',$data,true);
                            send_mail('info@foodrate.ir',$user->Email,"تایید ثبت نام در فود ریت",$message);
                            $this->create_session($user_profile);
                        } else {
                            $this->create_session($user_profile);
                        }
                        log_message('info', 'controllers.HAuth.login: user profile:' . PHP_EOL . print_r($user_profile, TRUE));
                        $data['user_profile'] = $user_profile;
                        $this->load->view('hauth/done', $data);
                    } else // Cannot authenticate user
                    {
                        show_error('Cannot authenticate user');
                    }
                } else // This service is not enabled.
                {
                    log_message('error', 'controllers.HAuth.login: This provider is not enabled (' . $provider . ')');
                    show_404($_SERVER['REQUEST_URI']);
                }
            } catch (Exception $e) {
                $error = 'Unexpected error';
                switch ($e->getCode()) {
                    case 0 :
                        $error = 'Unspecified error.';
                        break;
                    case 1 :
                        $error = 'Hybriauth configuration error.';
                        break;
                    case 2 :
                        $error = 'Provider not properly configured.';
                        break;
                    case 3 :
                        $error = 'Unknown or disabled provider.';
                        break;
                    case 4 :
                        $error = 'Missing provider application credentials.';
                        break;
                    case 5 :
                        log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
                        //redirect();
                        if (isset($service)) {
                            log_message('debug', 'controllers.HAuth.login: logging out from service.');
                            $service->logout();
                        }
                        show_error('User has cancelled the authentication or the provider refused the connection.');
                        break;
                    case 6 :
                        $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
                        break;
                    case 7 :
                        $error = 'User not connected to the provider.';
                        break;
                }

                if (isset($service)) {
                    $service->logout();
                }

                log_message('error', 'controllers.HAuth.login: ' . $error);
                show_error('Error authenticating user.');
            }
        } else {
            $user = new User();
            $result = $user->getByExample(array('Email' => $this->input->post('email'), 'pass' => sha1($this->input->post('pass'))));
            if ($result->num_rows() > 0) {
                $t = $result->result()[0];
                $u = new ArrayObject();
                $u->firstName = $t->FirstName;
                $u->lastName = $t->LastName;
                $u->email = $t->Email;
                $this->create_session($u);
            } else {
                echo '{"message":"<div class=\\"alert alert-danger persian\\">نام کاربری یا رمز ورود اشتباه می باشد</div>" }';
            }
        }
    }

    public function logOut()
    {
        $this->session->name = null;
        $this->session->email = null;
        $this->hybridauthlib->logoutAllProviders();
        redirect(base_url());

    }

    public function endpoint()
    {

        log_message('debug', 'controllers.HAuth.endpoint called.');
        log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: ' . print_r($_REQUEST, TRUE));

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
            $_GET = $_REQUEST;
        }

        log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
        require_once APPPATH . '/third_party/hybridauth/index.php';

    }

    private function create_session($user_profile)
    {
        $this->session->name = $user_profile->firstName . " " . $user_profile->lastName;
        $this->session->email = $user_profile->email;
        redirect(base_url());
    }

    public function test_user_add()
    {
        $user = new User();
        $user->ID = 19997;
        $res = $user->getById();
        if (sizeof($res) ==0) {
            $user->FirstName = "saman";
            $user->LastName = "salehi";
            $user->gender = 1;
            $user->age = 23;
            $user->Address = " تهران خیبابانن ازاددییی";
            $user->Email = "Saman@yahoo.com ";
            $user->Picurl = "http://localhost";
            $user->Describtion = "there is no describtion";
            $user->Phone = 2323232;
            $pass = create_rand_pass();
            $user->pass = sha1($pass);
            $user->insert();
            $data['pass'] =$pass ;
            $data['user'] = $user->Email;
            $message =  $this->load->view('confirm_mail',$data,true);
            send_mail('info@foodrate.ir','sami93sami93@yahoo.com',"slamale",$message);
        } else {
            $this->hybridauthlib->getSessionData();
            $this->session->name = "kavan";
            $this->session->email = "ddd1@yahoo.com";
            $_SESSION['test'] = "1234";
            redirect(base_url());
        }
    }
}

/* End of file hauth.php */
/* Location: ./application/controllers/hauth.php */
