<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->helper("url", "form");
        $this->load->model("Restaurant");
        $this->load->model("Other");
        $this->load->model("User");
        $this->load->library('session');
        $this->load->helper("utils");
    }

    public function index()
    {

        $data['jss'] = array("other");
//		$data['csss'] =array("s");
        $this->load->view('home', $data);
    }

    public function upload_image()
    {
        if ($this->input->post("dir_key") != null) {
            $dir_name = str_replace("@","-",str_replace(".","-",$this->input->post("dir_key")));
            if (!file_exists('./up/' . $dir_name)) {
                mkdir('./up/' . $dir_name);
            }
            $config['upload_path'] = './up/' . $dir_name;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $this->input->post("rest_name");
            $config['max_size'] = 40000;
//            $config['max_width'] = 1280;
//            $config['max_height'] = 960;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("file_data")) {
                $error = array('error' => $this->upload->display_errors());
                echo $error['error'];
            } else {
                $upload_data = $this->upload->data();
                //resize:
                $config['image_library'] = 'gd2';
                $config['source_image'] = $upload_data['full_path'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 800;
                $config['height'] = 600;

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
//                $data = array('upload_data' => $this->upload->data());
                echo '{"a":"success"}';
            }
        } else {
            show_404();
        }
    }

    public function rest_register($status = 0)
    {
        if ($status == 0) {
            $data = array("a" => "b");
            echo $this->load->view("rest_register", $data, true);
        } else if (($this->input->post("rest_name") != null)) {
            $rest = new Restaurant();
            $rest->rest_name = $this->input->post("rest_name");
            $rest->rest_address = $this->input->post("rest_address");
            $rest->rest_email = $this->input->post("rest_email");
            $rest->rest_phone = $this->input->post("phone_number");
            $rest->rest_description = $this->input->post("rest_description");
            $rest->rest_type = $this->input->post("rest_type");
            $rest->rest_pass = $this->input->post("rest_pass");

            $parking = $this->input->post("parking");
            $work_time = $this->input->post("work_time");
            $capacity = $this->input->post("capacity");
            $wifi = $this->input->post("wifi") != null ? "Wifi" : "";
            $live_music = $this->input->post("live_music") != null ? "LiveMusic" : "";
            $outdoor = $this->input->post("outdoor") != null ? "Outdoor" : "";
            $indoor = $this->input->post("indoor") != null ? "Indoor" : "";
            $peyk = $this->input->post("send_peyk") != null ? "SendPeyk" : "";
            $smoke = $this->input->post("smoke") != null ? "Smokeing" : "";
            $service_cost = $this->input->post("service_cost") != null ? "service_cost" : "";
            $birthday_service = $this->input->post("birthday_service") != null ? "BirthdayService" : "";
            $online_service = $this->input->post("online_service") != null ? "OnlineReserve" : "";

            $specs = $parking . "," . $work_time . "," . $capacity . "," . $wifi . "," . $live_music . "," . $outdoor . "," . $indoor . "," . $peyk . "," .
                $smoke . "," . $service_cost . "," . $birthday_service . "," . $online_service;

            $this->db->trans_start();
            $result = $rest->save();
            if ($result) {
                $this->_insert_specs($specs, $rest->ID);
            }
            $this->db->trans_complete();
            $this->_check_result($result);
        } else {
            show_404();
        }


    }

    public function load_rest_list($start = 0, $offset = 6)
    {
        if ($this->input->post("load") != null) {
            $rest = new Restaurant();
    		$results = $rest->getByExample(array("enabled"=>true),$offset,$start);
            foreach($results as $result) {
                $rest_list["id"] = $result->ID;
                $rest_list["rest_name"] = $result->rest_name;
                $rest_list["rest_address"] =$result->rest_address;
                $rest_list["rest_phone"] = $result->rest_phone;
                $rest_list["img_url"] = media_path($result->rest_email,$result->rest_name);
                echo $this->load->view("res_lists", $rest_list, true);
            }
            $data['page_size'] = 6;
            $data['size'] = $rest->count_all();
            $data['current'] =ceil($offset/$data["page_size"]);
            echo $this->load->view("pagination",$data,true);
        } else {
            show_404();
        }
    }


    public function user_signup()
    {
        if ($this->input->post("ajax") != null) {
            $data = array("a" => "b");
            echo $this->load->view("user_signup", $data, true);
        } else if ($this->input->post('email') != null) {
            $user = new User();
            $user->FirstName = $this->input->post('name');
            $user->LastName = $this->input->post('lastname');
            $user->Email = $this->input->post('email');
            $user->pass = sha1($this->input->post('pass'));
            if ($user->save())
            {
                echo '{"message":"<div class=\\"alert alert-success persian\\"> <strong>اطلاعات شما به درستی ثبت شد</strong><br>شما می توانید رستوران مورد نظر را انتخاب کنید و نظرات حود را ثبت کنید.</div>","location":"#userSignup"}';
            }
        } else {
            show_404();
        }
    }

    public function check_rest()
    {
        if ($this->input->post('ajax') != null) {
            $rest_email = $this->input->post('uname');
            $query = $this->Restaurant->getByExample(array('rest_email' => $rest_email));
            echo sizeof($query);
        } else {
            show_404();
        }
    }

    public function login($type = 1)
    {
        if ($this->input->post("ajax") != null) {
            $data = array("a" => "b");
            echo $this->load->view("login", $data, true);
        } else {
            show_404();
        }
    }

    private function _check_result($result)
    {
        if ($result) {
            echo '{"message":"<div class=\\"alert alert-success persian\\"> <strong>اطلاعات شما به درستی ثبت شد</strong><br> بعد از تایید مدیریت رستوران شما در الگوریتم رتبه بندی شرکت داده می شود. با تشکر</div>","location":"#rs"}';
        } else {
            if (strpos($this->db->error()['message'], "Duplicate") >= 0) {
                echo '{"message":"<div class=\\"alert alert-danger persian\\"><strong><br>خطا در ثبت اطلاعات</strong> اطلاعات وارد شده قبلا ثبت شده است</div>"}';
            }
        }
    }

    private function _insert_specs($specs, $rest_id)
    {
        $specs_map = $this->Other->get_all_spec();
        for ($i = 0; $i < sizeof($specs_map); $i++) {
            if (strpos($specs, $specs_map[$i]->FeatureName) && strpos($specs, $specs_map[$i]->FeatureName) >= 0) {
                $this->Other->save_specs(array("rest_id" => $rest_id, "specification_id" => $specs_map[$i]->ID));
            }
        }
    }
}

