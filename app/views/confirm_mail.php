<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Neopolitan Confirm Email</title>
    <link href="//db.onlinewebfonts.com/c/52ce4de2efeeb8b18dcbd379711224f3?family=B+Yekan" rel="stylesheet"
          type="text/css"/>
    <!-- Designed by https://github.com/kaytcat -->
    <!-- Robot header image designed by Freepik.com -->

    <style type="text/css">
        @import url(http://fonts.googleapis.com/css?family=Droid+Sans);
        @import url(//db.onlinewebfonts.com/c/52ce4de2efeeb8b18dcbd379711224f3?family=B+Yekan);

        @font-face {
            font-family: "B Yekan";
            src: url("//db.onlinewebfonts.com/t/52ce4de2efeeb8b18dcbd379711224f3.eot");
            src: url("//db.onlinewebfonts.com/t/52ce4de2efeeb8b18dcbd379711224f3.eot?#iefix") format("embedded-opentype"),
            url("//db.onlinewebfonts.com/t/52ce4de2efeeb8b18dcbd379711224f3.woff2") format("woff2"),
            url("//db.onlinewebfonts.com/t/52ce4de2efeeb8b18dcbd379711224f3.woff") format("woff"),
            url("//db.onlinewebfonts.com/t/52ce4de2efeeb8b18dcbd379711224f3.ttf") format("truetype"),
            url("//db.onlinewebfonts.com/t/52ce4de2efeeb8b18dcbd379711224f3.svg#B Yekan") format("svg");
        }

        /* Take care of image borders and formatting */

        img {
            max-width: 600px;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a {
            text-decoration: none;
            border: 0;
            outline: none;
            color: #bbbbbb;
        }

        a img {
            border: none;
        }

        /* General styling */

        td, h1, h2, h3 {
            font-family: 'B Yekan';
            font-weight: 400;
        }

        td {
            text-align: center;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100%;
            height: 100%;
            color: #37302d;
            background: #ffffff;
            font-size: 16px;
        }

        table {
            border-collapse: collapse !important;
        }

        .headline {
            color: #ffffff;
            font-size: 36px;
            font-family: "B Yekan";
        }

        .force-full-width {
            width: 100% !important;
        }

        .force-width-80 {
            width: 80% !important;
        }


    </style>

    <style type="text/css" media="screen">
        @media screen {
            /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
            td, h1, h2, h3 {
                font-family: 'B Yekan' !important;
            }
        }
    </style>

    <style type="text/css" media="only screen and (max-width: 480px)">
        /* Mobile styles */
        @media only screen and (max-width: 480px) {

            table[class="w320"] {
                width: 320px !important;
            }

            td[class="mobile-block"] {
                width: 100% !important;
                display: block !important;
            }

        }
    </style>
</head>
<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
      bgcolor="#ffffff">
<table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
    <tr>
        <td align="center" valign="top" bgcolor="#ffffff" width="100%">
            <center>
                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                    <tr>
                        <td align="center" valign="top">

                            <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width"
                                   style="margin:0 auto;">
                                <tr>
                                    <td style="font-size: 30px; text-align:center;">
                                        <br>
                                        <a href="www.foodrate.ir">www.foodrate.ir</a>
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                            </table>

                            <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width"
                                   bgcolor="#4dbfbf">
                                <tr>
                                    <td>
                                        <br>
                                        <img src="http://foodrate.ir/res/img/logo.png" width="130" height="170"
                                             alt="robot picture">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="headline">
                                        به جمع ما خوش آمدید
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                        <center>
                                            <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="60%">
                                                <tr>
                                                    <td style="color:#187272;">
                                                        <br>
                                                        ثبت نام شما به درستی انجام شده و با استفاده از اطلاعات داده شده
                                                        می توانید وارد پنل کاربری خود شوید
                                                        <br>
                                                        <br>
                                                    </td>
                                                </tr>
                                            </table>
                                        </center>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div><!--[if mso]>
                                            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                         xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
                                                         style="height:50px;v-text-anchor:middle;width:200px;"
                                                         arcsize="8%" stroke="f" fillcolor="#178f8f">
                                                <w:anchorlock/>
                                                <center>
                                            <![endif]-->
                                            <a href="http://www.foodrate.ir"
                                               style="background-color:#178f8f;border-radius:4px;color:#ffffff;display:inline-block;font-family: 'B Yekan';font-size:16px;font-weight:bold;line-height:50px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;">ورود
                                                به پنل کاربری</a>
                                            <!--[if mso]>
                                            </center>
                                            </v:roundrect>
                                            <![endif]--></div>
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                            </table>

                            <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width"
                                   bgcolor="#f5774e">
                                <tr>
                                    <td style="background-color:#f5774e;">

                                        <center>
                                            <table style="margin:0 auto;" cellspacing="0" cellpadding="0"
                                                   class="force-width-80">
                                                <tr>
                                                    <td style="text-align:left; color:#933f24">
                                                        <br>
                                                        <br>
                                                        <span style="color:#ffffff;">نام کاربری</span> <br>
                                                        <?php echo $user; ?> <br>
                                                    </td>
                                                    <td style="text-align:right; vertical-align:top; color:#933f24">
                                                        <br>
                                                        <br>
                                                        <span style="color:#ffffff;">رمز عبور</span> <br>
                                                        <?php echo $pass; ?>
                                                    </td>
                                                </tr>
                                            </table>


                                            <table style="margin:0 auto;" cellspacing="0" cellpadding="0"
                                                   class="force-width-80">
                                                <tr>
                                                    <td class="mobile-block">
                                                        <br>
                                                        <br>


                                                        <br>
                                                    </td>
                                                </tr>
                                            </table>


                                            <table style="margin: 0 auto;" cellspacing="0" cellpadding="0"
                                                   class="force-width-80">
                                                <tr>
                                                    <td style="text-align:left; color:#933f24;">
                                                        <br>
                                                       با تشکر از حسن انتخاب شما، امیدواریم که بتوانیم رضایت شما را جلب کنیم.
                                                        <br>
                                                        <br>
                                                        اعضای فودریت
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </td>
                                                </tr>
                                            </table>
                                        </center>
                                    </td>
                                </tr>
                            </table>

                            <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width"
                                   bgcolor="#414141" style="margin: 0 auto">
                                <tr>
                                    <td style="background-color:#414141;">
                                        <br>
                                        <br>
                                        <a href="https://www.instagram.com/foodrate.ir"><img
                                                src="http://foodrate.ir/res/img/instagram.png" alt="instagram"></a>
                                        <a href="http://telegram.me/foodrate.ir"><img
                                                src="http://foodrate.ir/res/img/telegram.png" alt="telegram"></a>
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color:#bbbbbb; font-size:12px;">
                                        © 2016 foodrate.ir
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
</body>
</html>