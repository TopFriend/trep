<?php $this->load->view("home_start"); ?>
<?php $this->load->view("home_header"); ?>

<?php //$this->load->view("login")?>
<?php //$this->load->view("rest-register") ?>
<?php //$this->load->view("user_signup") ?>

<div id="modal"></div>
<div id="register"></div>
<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading persian-titr">سرویس ها</h2>
                <h3 class="section-subheading text-muted persian">سرویس های قابل ارائه توسط سایت ما</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lightbulb-o fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading persian">پیشنهاد</h4>

                <p class="text-muted persian">قدم نهای استفاده از الگوریتم های یادگیری برای پیشنهاد دادن بهترین رستوران
                    ها به
                    کاربران می باشد.</p>
            </div>
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-sort-amount-asc fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading persian">رتبه بندی</h4>
                <p class="text-muted persian">هدف بعدی رتبه بندی رستوران ها بر اساس نظرات مشتریان و امکانات رستوران ها
                    می
                    باشد.</p>
            </div>
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-cutlery fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading persian">جمع آوری</h4>

                <p class="text-muted persian">هدف اصلی سایت جمع آوری اطلاعات مربوط به تمام رستوران های ایران و دسته بندی
                    آن ها
                    بر اساس نوع رستوران می باشد.</p>
            </div>
        </div>
    </div>
</section>

<!-- Portfolio Grid Section -->
<section id="portfolio" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading persian persian-titr">رتبه بندی</h2>
                <h3 class="section-subheading text-muted persian">در زیر لیستی از بهترین رستوران ها با رتبه آورده شده
                    است.</h3>
            </div>
        </div>
        <div class="row" id="rest_lists">
            <img class="loadding" src="<?php echo base_url('res/img/loading.gif') ?>">
        </div>
    </div>
</section>


<!-- About Section -->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading persian-titr">راهنمای استفاده</h2>
                <h3 class="section-subheading text-muted persian">برای آشنایی بیشتر قسمت زیر را مطالعه بفرمایید</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="res/img/about/1.png" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4></h4>
                                <h4 class="subheading persian">ثبت رستوران</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted persian text-justify">صاحبان رستوران ها می توانند با ثبت اطلاعات
                                    مربوط به رستوران خود در
                                    سایت
                                    در رتبه بندی شرکت کنند.
                                    پس از تایید اطلاعات مربوط به رستوران توسط مدیر سایت، رستوران در الگوریتم رتبه بندی
                                    قرار می گیرد. و هر چه کاربران نظرات مثبت در مورد رستوران ارائه دهند در رتبه بالاتر
                                    قرار می گیرد.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="res/img/about/2.png" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4></h4>
                                <h4 class="subheading text-right persian">معرفی به مشتریان</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted persian text-justify">صاحبان رستوران ها می توانند با معرفی برنامه
                                    به مشتریان خود این
                                    امکان را فراهم کنند
                                    که تعداد بازدید کننده انها بیشتر شده و در رتبه بالاتر قرار گیرند.
                                    چون تعداد نظرات هم خود به عنوان یک پارامتر مثبت در رنکینگ تاثیر داده می شود.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="res/img/about/3.png" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4></h4>
                                <h4 class="subheading persian">نظرات کاربران</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted persian text-justify">کاربران یا به عبارت دیگر مشتریان رستوران ها
                                    می توانند با ثبت نام
                                    در سایت و یا وارد کردن جی میل و یا ایمیل لاگین کرده و
                                    نظرات خود را به سادگی با کلیک کردن به روی سرویس ارائه شده توسط رستوران ثبت کنند. با
                                    هر بار کلیک کردن بر روی سروس مورد نظر به امتیاز آن اضافه می شود.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="res/img/about/4.png" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4></h4>
                                <h4 class="subheading text-right persian">پیشنهاد</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted persian text-justify">با توجه به سابقه فعالیت کاربران در سایت،
                                    بهترین رستوران ها به آن
                                    ها پیشنهاد داده می شود.
                                    مثلا با توجه به اینکه کاربر به چه پارامترهای نظر مثبت یا منفی داده می توان فهمید که
                                    چه رستوران های ممکن است مد نظر کاربر باشد.z</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4 class="persian">شما هم به<br> ما بپیوندید

                                <br></h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading persian">ارتباط با ما</h2>

                <h3 class="section-subheading text-muted persian">خوشحال میشیم پیشنهادات و نظرات خودتون رو با ما در میان
                    بگذارید.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control persian" placeholder="متن پیام شما *" id="message"
                                          required
                                          data-validation-required-message="Please enter a message."></textarea>

                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control persian" placeholder="نام *" id="name" required
                                       data-validation-required-message="لطفا نام خود را وارد کنید">

                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control persian" placeholder="آدرس ای میل *" id="email"
                                       required
                                       data-validation-required-message="لطف ایمیلتان را وارد کنید.">

                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control persian" placeholder="شماره تلفن *" id="phone"
                                       required
                                       data-validation-required-message="Please enter your phone number.">

                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl persian">ارسال پیام</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Copyright &copy; foodrate.ir 2016</span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li><a href="https://www.instagram.com/foodrate.ir/"><i class="fa fa-instagram"></i></a>
                    </li>
                    <li><a href="http://telegram.me/foodrate.ir"><i class="fa fa-phone"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li><a href="#">Privacy Policy</a>
                    </li>
                    <li><a href="#">Terms of Use</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- /.container -->
<?php $this->load->view("home_end"); ?>
<script type="text/javascript">
    $(document).ready(function () {
        ajaxPost('<?php echo base_url("home/load_rest_list"); ?>', 'load=1', 'post', '#rest_lists');
    });
</script>


