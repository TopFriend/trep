<div class="modal fade" tabindex="-1" id="loginbox" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                     <button class="close pull-left" data-dismiss="modal" type="button">s</button>
                <div class="modal-title pull-right persian">ورود به پنل کاربری</div>
            </div>
            <div class="modal-body">

                <div class="clearfix"></div>
                <div class="tab-content">
                    <form id="loginform" action="<?php echo base_url('hauth/login') ?>" method="post" data-async data-target="#resultShow" class="tab-pane fade in active" role="form">

                        <div  class="input-group">
                            <input id="login-username" type="text" class="form-control" name="email" validate value=""
                                   placeholder="نام کاربری یا پست الکترونیکی">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        </div>
                        <div  class="input-group">
                            <input id="login-password" type="password" class="form-control" validate name="pass"
                                   placeholder="رمز ورود">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        </div>
                        <div class="input-group">
                            <div class="checkbox">
                                <label>
                                    <input id="login-remember" type="checkbox" name="remember" value="1">
                                    من رو به خاطر بسپار
                                </label>
                            </div>
                        </div>
                        <div id="resultShow"></div>
                        <div class="form-group">
                            <!-- Button -->
                            <div class=" controls">
                                <input type="submit" id="btn-login"  class="btn btn-success" value="ورود"> </input>
                                <a id="btn-fblogin" href="<?php echo base_url('hauth/login/Google')?>" class="btn btn-danger"><i class="fa fa-google"></i> ورود با جی میل  </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#loginbox').modal();
        $('.registration-form fieldset:first-child').fadeIn('slow');
    });
</script>
