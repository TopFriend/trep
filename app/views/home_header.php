<?php
/**
 * Created by IntelliJ IDEA.
 * User: ssalehi
 * Date: 6/13/2016
 * Time: 10:43 AM
 */
?>
<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top"><img src = "<?php echo  base_url('res/img/logo.png')?>" class="img img-responsive"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle persian" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span> <i class="fa fa-1x fa-user"></i> ثبت</a>
                    <ul class="dropdown-menu">
                        <li><a  href="#rs" onclick="ajaxPost(base_url+'home/rest_register', 'ajax=1', 'post', '#register','#rs')" class="ajax">ثبت رستوران <i class="fa fa-cutlery" ></i></a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#userSignup" onclick="ajaxPost(base_url+'home/user_signup', 'ajax=1', 'post', '#register','#userSignup')">ثبت کاربر <i class="fa fa-users" ></i></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle persian" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span> <i class="fa fa-1x fa-sign-in"></i> ورود</a>
                    <ul class="dropdown-menu" id="signin">
                        <li><a  href="<?php echo base_url("home/login"); ?>" class="ajax">ورود به پنل کاربری <i class="fa fa-male" ></i></a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#" data-toggle="modal" data-target="#signupbox">ورود به پنل رستوران <i class="fa fa-lemon-o" ></i></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </li>
                <li>
                  <a class="page-scroll persian" href="#contact"> <i class="fa fa-1x fa-phone"></i> تماس  </a>
                </li>
                <li>
                    <a class="page-scroll persian" href="#about"><i class="fa fa-1x fa-question"></i> راهنمایی  </a>
                </li>
                <li>
                    <a class="page-scroll persian" href="#portfolio"><i class="fa fa-1x fa-bar-chart"></i> رتبه  </a>
                </li>
                <li>
                    <a class="page-scroll persian" href="#services"><i class="fa fa-1x fa-wrench"></i> سرویس  </a>
                </li>
                <?php if ($this->session->name!=null) { ?>
                <li class="dropdown" >
                    <a href="#" class="dropdown-toggle persian" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span><i class="fa fa-cogs"></i> </a>
                    <ul class="dropdown-menu" id="setting">
                        <li><a href="<?php echo base_url('settings/edit_rest_info') ?>" class="ajax">تغییر اطلاعات رستوران  <i class=" fa fa-pencil" ></i></a></li>
                        <li><a href="<?php echo base_url("settings/edit_pass"); ?>" class="ajax">تغییر رمز ورود  <i class=" fa fa-lock" ></i></a></li>
                        <li><a href="#"></a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo base_url('hauth/logOut') ?>" > خروج <i class=" fa fa-sign-out" ></i></a></li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->
<header>
    <div class="container" >
        <div class="intro-text"><br>
            <div class="intro-lead-in"><!--Coming Soon !--></div>
            <div class="intro-heading">به زودی با رتبه بندی رستوران های تهران در<br> خدمت شما خواهیم بود</div>
            <a href="#rs" onclick="ajaxPost(base_url+'home/rest_register', 'ajax=1', 'post', '#register','#rs')"   title="جزو اوبین کسانی باشید که رستوران خود را در اولین سایت رتبه بندی رستوران ایران ثبت می کنند" class="btn btn-info persian ajax" >همین حالا ثبت کنید</a>
        </div>
    </div>
</header>

