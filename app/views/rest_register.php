<?php
/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 8/27/2016
 * Time: 10:12 AM
 */ ?>

<!--<div id="restRegister" class="modal fade" role="dialog">-->
<!--    <div class="modal-dialog">-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-header">-->
<!--                <button class="close pull-left" type="button" data-dismiss="modal">&times;</button>-->
<!--                <div class="modal-title text-right persian">ثبت نام رستوران</div>-->
<!--            </div>-->
<!--            <div class="modal-body">-->
<section class="bg-light-gray" id="rs">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading persian-titr">ثبت رستوران</h2>

                <h3 class="section-subheading text-muted persian">برای پیوستن به ما و شرکت در الگوریتم رتبه بندی لطفا
                    رستوران خود را ثبت کنید</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 text-center col-lg-push-2">
                <form role="form" action="<?php echo base_url('home/rest_register/1') ?>" method="post"
                      class="registration-form" data-async data-target='#message_alert'>
                    <fieldset>
                        <div class="form-top">
                            <div class="form-top-right">
                                <h3 class="text-right persian">1 / 4</h3>

                                <p class="persian">مشخصات کاربری</p>
                            </div>
                            <div class="form-top-left">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <div class="input-group form-group">
                                <label class="sr-only" for="rest_email">پست الکترونیکی</label>
                                <input type="text" name="rest_email" placeholder="پست الکترونیکی"
                                       class="form-control text-right" id="rest_email" validate>
                                <span class="input-group-addon"><i class="fa fa-envelope"></i> </span>
                            </div>
                            <div class="input-group form-group">
                                <label class="sr-only" for="rest_pass">کلمه عبور</label>
                                <input type="password" name="rest_pass" placeholder="کلمه عبور"
                                       class=" form-control text-right" id="rest_pass" validate>
                                <span class="input-group-addon"><i class="fa fa-key"></i> </span>
                            </div>
                            <div class="input-group form-group">
                                <label class="sr-only" for="form_repeat_password">تکرا کلمه عبور</label>
                                <input type="password" name="form_repeat_password" placeholder="تکرار کلمه عبور"
                                       class=" form-control text-right " id="form_repeat_password" validate>
                                <span class="input-group-addon"><i class="fa fa-key"></i> </span>
                            </div>
                            <div id="alert"></div>
                            <button id="step1" type="button" class="btn btn-next btn-info">بعد</button>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-top">
                            <div class="form-top-right">
                                <h3 class="text-right persian"> 2 / 4</h3>

                                <p class="persian">اطلاعات کلی</p>
                            </div>
                            <div class="form-top-left">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <div class="input-group form-group pull-right">
                                <select class="selectpicker" name="rest_type" title="نوع رستوران">
                                    <option value="iran_rest">رستوران ایرانی</option>
                                    <option value="foreign_rest">رستوران خارجی</option>
                                    <option value="hotel_rest">هتل رستوران</option>
                                    <option value="fastfood">فست فود</option>
                                </select>
                                <!--                                <span class="input-group-addon"><i class="fa fa-book"></i> </span>-->
                            </div>
                            <div class="input-group form-group">
                                <label class="sr-only" for="rest_name">Restaurant name</label>
                                <input type="text" name="rest_name" placeholder="نام رستوران"
                                       class="form-control text-right" id="rest_name" validate>
                                <span class="input-group-addon"><i class="fa fa-cutlery"></i> </span>
                            </div>
                            <div class="input-group form-group">
                                <label class="sr-only" for="phone_number">Phone Number</label>
                                <input type="tel" name="phone_number" placeholder="شماره تلفن"
                                       class="form-control text-right" id="phone_number" validate>
                                <span class="input-group-addon"><i class="fa fa-phone"></i> </span>
                            </div>
                            <div class="input-group form-group">
                                <label class="sr-only" for="rest_address">آدرس</label>
				                        	<textarea name="rest_address" placeholder="آدرس ..."
                                                      class="form-control text-right"
                                                      id="rest_address" validate></textarea>
                                <span class="input-group-addon"><i class="fa fa-location-arrow"></i> </span>
                            </div>
                            <button type="button" class="btn btn-previous btn-success">قبل</button>
                            <button type="button" class="btn btn-next btn-info">بعد</button>

                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-top">
                            <div class="form-top-right">
                                <h3 class=" persian text-right">3 / 4</h3>

                                <p class="persian">امکانات رستوران</p>
                            </div>
                            <div class="form-top-left">
                                <i class="fa fa-beer"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <select class="selectpicker" name="parking" id="parking" title="وضعیت پارکینگ">
                                        <option value="NearRestParking">محل پارک مناسب در اطراف رستوران</option>
                                        <option value="Metro">دسترسی به مترو</option>
                                        <option value="BRT">دسترسی به بی آر تی</option>
                                        <option value="SelfParkingArea">پارکینگ اختصاصی</option>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <select class="selectpicker" multiple data-selected-text-format="count > 3"
                                            name="work_time" title="تایم کاری">
                                        <option value="Breakfast">صبحانه</option>
                                        <option value="Lunch">نهار</option>
                                        <option value="Dinner">شام</option>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <select name="capacity" class="selectpicker" title="ظرفیت">
                                        <option value="c<15">کمتر از 15 نفر</option>
                                        <option value="c<30">کمتر از 30 نفر</option>
                                        <option value="c<60">کمتر از 60 نفر</option>
                                        <option value="c>60">بالای 60 نفر</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <input type="checkbox" name="wifi"

                                           autocomplete="off" id="wifi"/>

                                    <div class="[ btn-group ]">
                                        <label for="wifi" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="wifi"
                                               class="[ btn btn-default active ]">
                                            (wifi) وای فای
                                            <span class="glyphicon glyphicon-signal"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="checkbox" name="live_music"
                                           id="livemusic"
                                           autocomplete="off"/>

                                    <div class="[ btn-group ]">
                                        <label for="livemusic" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="livemusic"
                                               class="[ btn btn-default active ]">
                                            موسیقی زنده<span class="glyphicon glyphicon-music"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="checkbox" name="online_services"
                                           id="onlinereserve"
                                           autocomplete="off"/>

                                    <div class="[ btn-group ]">
                                        <label for="onlinereserve" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="onlinereserve"
                                               class="[ btn btn-default active ]">
                                            رزور آنلاین<span class="glyphicon glyphicon-credit-card"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="checkbox" name="outdoor"
                                           id="outdoor"
                                           autocomplete="off"/>

                                    <div class="[ btn-group ]">
                                        <label for="outdoor" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="outdoor"
                                               class="[ btn btn-default active ]">فضای
                                            خارج <span class="glyphicon glyphicon-tree-deciduous"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group ">
                                    <input type="checkbox" name="indoor"
                                           id="indoor"
                                           autocomplete="off"/>

                                    <div class="[ btn-group ]">
                                        <label for="indoor" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="indoor"
                                               class="[ btn btn-default active ]">
                                            فضای داخلی<span class="glyphicon glyphicon-home"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="checkbox" name="send_peyk"
                                           id="sendpeyk"
                                           autocomplete="off"/>

                                    <div class="[ btn-group ]">
                                        <label for="sendpeyk" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="sendpeyk"
                                               class="[ btn btn-default active ]">
                                            پیک ارسال<span class="glyphicon glyphicon-shopping-cart"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="checkbox" name="smoke"
                                           id="smoke"
                                           autocomplete="off"/>

                                    <div class="[ btn-group ]">
                                        <label for="smoke" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="smoke"
                                               class="[ btn btn-default active ]">کشیدن سیگار<span
                                                class="glyphicon glyphicon-fire"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="checkbox" name="birthday_service"
                                           id="birthday_service"
                                           autocomplete="off"/>

                                    <div class="[ btn-group ]">
                                        <label for="birthday_service" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="birthday_service"
                                               class="[ btn btn-default active ]">سرویس جشن تولد<span
                                                class="glyphicon glyphicon-pawn"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="checkbox" name="service_cost"
                                           id="service_cost"
                                           autocomplete="off"/>

                                    <div class="[ btn-group ]">
                                        <label for="service_cost" class="[ btn btn-default ]">
                                            <span class="[ glyphicon glyphicon-ok ]"></span>
                                            <span> </span>
                                        </label>
                                        <label for="service_cost"
                                               class="[ btn btn-default active ]">پرداخت آنلاین<span
                                                class="glyphicon glyphicon-euro"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group form-group">
                                <label class="sr-only" for="web_address">ادرس وب سایت</label>
                                <input type="text" name="form-google-plus" placeholder="آدرس وب سایت"
                                       class="form-google-plus form-control text-right" id="form-google-plus">
                                <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="rest_description">توضیحات</label>
				                        	<textarea name="rest_description" placeholder="توضیحات"
                                                      class="form-control text-right"
                                                      id="rest_description"></textarea>
                            </div>
                            <button type="button" class="btn btn-previous btn-success">قبل</button>
                            <button type="button" class="btn btn-next btn-info" id="next_page">بعد</button>
                    </fieldset>
                    <fieldset>
                        <div class="form-bottom">
                            <?php $this->load->view("image-upload"); ?>
                            <button type="button" class="btn btn-previous btn-success">قبل</button>
                            <button id="popoverData" type="submit" class="btn btn-next btn-info" data-trigger="hover"
                                    data-content="لطفا قبل از ثبت نهایی همه عکس های انتخابی را آپلود کنید" rel="popover"
                                    data-placement="top">ثبت نهایی
                            </button>
                        </div>
                    </fieldset>
                </form>
                <div id="message_alert"></div>
            </div>
        </div>
    </div>
</section>
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<script type="text/javascript">
    $(document).ready(function () {
        $('#restRegister').modal();
        $('.registration-form fieldset:first-child').fadeIn('slow');
        $('.selectpicker').selectpicker('refresh');  //refresh selectpicker
        $('.file').fileinput('reset');               //refresh fileinput

        $('#rest_email').focusout(function () {
            $.ajax({
                url: base_url + "home/check_rest/",
                data: "ajax=1&uname="+ $('#rest_email').val(),
                type: "post",
                success: function (result, status) {
                    if (result == "1") {
                        $('#alert').html('<div class="alert alert-danger text-center persian"><strong>خطا در ثبت اطلاعات<br></strong> اطلاعات وارد شده قبلا ثبت شده است</div>');
                        $("#step1").attr("Disabled", true);
                    } else {
                        $("#step1").attr("Disabled", false);
                        $('#alert').html('');
                    }
                },
                complete: function (result) {
                }
            });
        });
        $('#popoverData').popover({trigger: "hover"});
        $('#popoverData').prop('disabled', true);
        $(document.body).on('click', '.fileinput-upload-button', function () {
            $('#popoverData').prop('disabled', false);
        });
    })
</script>

