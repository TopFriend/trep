<?php
/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 9/25/2016
 * Time: 9:31 AM
 */ ?>
<section class="bg-light-gray" id="userSignup">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading persian-titr">ثبت کاربر</h2>

                <h3 class="section-subheading text-muted persian">برای نظر دادن و استفاده از امکانات فودریت لطفا ثبت نام کنید.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 text-center col-lg-push-2">
                    <form id="signupform" class="registration-form" role="form" method="post" action="<?php echo base_url('home/user_signup')?>" data-async  data-target='#message_alert'>
                        <div id="signupalert" style="display:none" class="alert alert-danger">
                            <p>Error:</p>
                            <span></span>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" name="email"
                                   validate   placeholder="پست الکترونیکی">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" name="name"
                                   validate   placeholder="نام">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" name="lastname"  placeholder="نام خانوادگی">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                        </div>
                        <div class="input-group">
                            <input type="password" id="user_pass" class="form-control" name="pass"
                                   validate placeholder="رمز ورود">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        </div>
                        <div class="input-group">
                            <input type="password" id="user_repeat_pass" class="form-control" name="repasswd"
                                   validate  placeholder="تکرار رمز عبور">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        </div>


                        <div class="form-group">
                            <!-- Button -->
                            <div class=" controls">
                                <input type="submit" id="btn_user_register"  class="btn btn-success"  value="ثبت"> </input>
                                <a id="btn-fblogin" href="<?php echo base_url('hauth/login/Google')?>" class="btn btn-danger"><i class="fa fa-google"></i> ثبت نام با جی میل</a>
                            </div>
                        </div>

                    </form>
                <div id="message_alert"></div>
            </div>
        </div>
    </div>
</section>

