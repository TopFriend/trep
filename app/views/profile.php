<?php $this->load->view("profile_start"); ?>
<?php $this->load->view("profile_header"); ?>

    <!-- About -->
    <section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php echo $profile_info->rest_description ?></h2>

                    <p class="lead">با ما باشید تا بهترین رستوران های تهران را به شما معرفی کنیم</p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Services -->
    <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
    <section id="services" class="services bg-primary">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2>سرویس های ارائه شده</h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-2 col-sm-4">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-cutlery fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>کیفیت</strong>
                                </h4>

                                <p>آیا از کیفیت غذا راضی بودید؟</p>
                                <a href="#" class="btn">
                                    <div class="c100 p50 small" align="center">
                                        <span>50%</span>

                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-map-marker fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>مکان</strong>
                                </h4>

                                <p>رستوران در محل مناسبی قرار گرفته است؟
                                </p>
                                <a href="#" class="btn">
                                    <div class=" c100 p60 small green" align="center">
                                        <span>60%</span>

                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-wifi fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>اینترنت</strong>
                                </h4>

                                <p>از سرعت اینترنت و امکانات آن راضی بودید؟</p>
                                <a href="#" class="btn" data-content="Popup with option trigger" rel="popover"
                                   data-placement="bottom" data-original-title="Title">
                                    <div class=" c100 p20 small orange">
                                        <span>20%</span>

                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-shield fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>برخورد پرسنل</strong>
                                </h4>

                                <p>از نحوه برخورد و پذیرای پرسنل راضی بودید؟</p>
                                <a id="popoverData" class="btn" href="#" data-content="Popover with data-trigger"
                                   rel="popover" data-placement="bottom" data-original-title="Title"
                                   data-trigger="hover">
                                    <div class=" c100 p50 small">
                                        <span>50%</span>

                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-music fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>موسیقی</strong>
                                </h4>

                                <p>از موسیقی زنده لذت بردین؟</p>
                                <a href="#" class="btn" data-content="Popup with option trigger" rel="popover"
                                   data-placement="bottom" data-original-title="Title">
                                    <div class=" c100 p20 small orange">
                                        <span>20%</span>

                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-credit-card fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>هزینه</strong>
                                </h4>

                                <p>آیا نسبت به خدمات ارائه شده از هزینه ها راضی بودین؟</p>
                                <a href="#" class="btn" data-content="Popup with option trigger" rel="popover"
                                   data-placement="bottom" data-original-title="Title">
                                    <div class=" c100 p20 small orange">
                                        <span>20%</span>

                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Callout -->
<!--    <aside class="callout">-->
<!--        <div class="text-vertical-center">-->
<!--            <h2>Vertically Centered Text</h2>-->
<!---->
<!--            <div class="container">-->
<!--                <div class="row">-->
<!--                    <div class="col-sm-8 col-sm-push-2">-->
<!--                        <div class="panel panel-white post panel-shadow">-->
<!--                            <div class="post-heading">-->
<!--                                <div class="pull-left image">-->
<!--                                    <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar"-->
<!--                                         alt="user profile image">-->
<!--                                </div>-->
<!--                                <div class="pull-left meta">-->
<!--                                    <div class="title h5">-->
<!--                                        <a href="#"><b>Ryan Haywood</b></a>-->
<!--                                        made a post.-->
<!--                                    </div>-->
<!--                                    <h6 class="text-muted time">1 minute ago</h6>-->
<!--                                </div>-->
<!--                                <div class="post-description">-->
<!--                                    <label> snippets resoor bootstrap cpets resostrap cpets resources nd utilities for-->
<!--                                        bootstrap cpets resoootstrap css hmtl js framework. Cod</label>-->
<!--                                </div>-->
<!--                                <div class="stats">-->
<!--                                    <a href="#" class="btn btn-xs btn-primary stat-item">-->
<!--                                        <i class="fa fa-thumbs-up icon"></i>2-->
<!--                                    </a>-->
<!--                                    <a href="#" class="btn btn-xs btn-primary  stat-item">-->
<!--                                        <i class="fa fa-thumbs-down icon"></i>12-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-sm-8 col-sm-push-2">-->
<!--                        <div class="panel panel-white post panel-shadow">-->
<!--                            <div class="post-heading">-->
<!--                                <div class="pull-left image">-->
<!--                                    <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar"-->
<!--                                         alt="user profile image">-->
<!--                                </div>-->
<!--                                <div class="pull-left meta">-->
<!--                                    <div class="title h5">-->
<!--                                        <a href="#"><b>Ryan Haywood</b></a>-->
<!--                                        made a post.-->
<!--                                    </div>-->
<!--                                    <h6 class="text-muted time">1 minute ago</h6>-->
<!--                                </div>-->
<!--                                <div class="post-description">-->
<!--                                    <label> snippets resources nd utilities for bootstrap cpets resources nd utilities-->
<!--                                        for bootstrap cpets resources nd utilities for bootstrap cpets resources nd-->
<!--                                        utilities for bootstrap cpets resources nd utilities for bootstrap css hmtl js-->
<!--                                        framework. Cod</label>-->
<!--                                </div>-->
<!--                                <div class="stats">-->
<!--                                    <a href="#" class="btn btn-xs btn-primary stat-item">-->
<!--                                        <i class="fa fa-thumbs-up icon"></i>2-->
<!--                                    </a>-->
<!--                                    <a href="#" class="btn btn-xs btn-primary  stat-item">-->
<!--                                        <i class="fa fa-thumbs-down icon"></i>12-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="row" style="padding-bottom: 20px">-->
<!--                    <div class="col-sm-8 col-sm-push-2">-->
<!--                        <div class="input-group">-->
<!--                            <input type="text" placeholder="Write Comment..." class="form-control">-->
<!--                            <span class="input-group-btn">-->
<!--                                 <button type="button" class="btn btn-primary">Add</button>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        </div>-->
<!--    </aside>-->

    <!-- Portfolio -->
    <section id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h2>گالری</h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-6 portfolio-item">
                            <a href="<?php echo "#portfolioModal" . $profile_info->ID ?>" class="portfolio-link"
                               data-toggle="modal" >
                                <div class="caption">
                                    <div class="caption-content">
                                        <i class="fa fa-search-plus fa-3x"></i>
                                    </div>
                                </div>
                                <img class="img-portfolio img-responsive" alt="<?php echo $profile_info->rest_name; ?>"
                                     src="<?php echo media_path($profile_info->rest_email, $profile_info->rest_name) ?>">
                            </a>
                        </div>

                        <?php for ($i = 1; $i <= 3; $i++) { ?>
                            <div class="col-md-6 portfolio-item">
                                <a href="#portfolioModal<?php echo $profile_info->ID.$i ?>" class="portfolio-link"
                                   data-toggle="modal">
                                    <div class="caption">
                                        <div class="caption-content">
                                            <i class="fa fa-search-plus fa-3x"></i>
                                        </div>
                                    </div>
                                    <img class="img-portfolio img-responsive"
                                         alt="<?php echo $profile_info->rest_name; ?>"
                                         src="<?php echo media_path($profile_info->rest_email, $profile_info->rest_name . $i) ?>">
                                </a>
                            </div>
                        <?php } ?>
                        <!-- /.row (nested) -->
                        <a href="#" class="btn btn-dark">مشاهده تصاویر بیشتر</a>
                    </div>


                    <!-- /.col-lg-10 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
    </section>

    <!-- Call to Action -->
    <aside class="call-to-action bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <!--                    <h3>The buttons below are impossible to resist.</h3>-->
                    <!--                    <a href="#" class="btn btn-lg btn-light">Click Me!</a>-->
                    <!--                    <a href="#" class="btn btn-lg btn-dark">Look at Me!</a>-->
                </div>
            </div>
        </div>
    </aside>

    <!-- Map -->
    <section id="contact" class="map">
        <!--        <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"-->
        <!--                src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=22.659344,-1.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>-->
        <!--        <br/>-->
        <!--        <small>-->
        <!--            <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=22.659344,81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a>-->
        <!--        </small>-->
        <!--        </iframe>-->
        <div id="googleMap" style="width:100%;height:600px;"></div>
    </section>
    <div class="portfolio-modal modal fade" id="portfolioModal<?php echo $profile_info->ID; ?>" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2 class="persian titr"> <?php echo $profile_info->rest_name; ?></h2>
                            <hr class="star-primary">
                            <img src="<?php echo media_path($profile_info->rest_email,$profile_info->rest_name) ?>"
                                 class="img-responsive img-centered" alt="">

                            <p class="persian text-justify"><?php echo $profile_info->rest_description; ?></p>
                            <ul class="list-inline item-details">
                                <!-- <li>Client:
                                     <strong><a href="http://startbootstrap.com">Start Bootstrap</a>
                                     </strong>
                                 </li>-->
                                <li>تلفن:
                                    <strong><a href="#"><?php echo $profile_info->rest_phone; ?></a>
                                    </strong>
                                </li>

                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"
                                    class="fa fa-times">
                                بستن
                            </button>
                            <!-- <form action="<?php /*echo base_url("home/like/$item->id"); */?>" method="post" data-async
                                  data-target="#like">
                                <button type="submit" class="btn btn-default">Like <span class="badge" id="like">0</span></button>
                            </form>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php for ($i = 1; $i <= 3; $i++) { ?>
    <div class="portfolio-modal modal fade" id="portfolioModal<?php echo $profile_info->ID.$i; ?>" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2 class="persian titr"> <?php echo $profile_info->rest_name; ?></h2>
                            <hr class="star-primary">
                            <img src="<?php echo media_path($profile_info->rest_email,$profile_info->rest_name.$i) ?>"
                                 class="img-responsive img-centered" alt="">

                            <p class="persian text-justify"><?php echo $profile_info->rest_description; ?></p>
                            <ul class="list-inline item-details">
                                <!-- <li>Client:
                                     <strong><a href="http://startbootstrap.com">Start Bootstrap</a>
                                     </strong>
                                 </li>-->
                                <li>تلفن:
                                    <strong><a href="#"><?php echo $profile_info->rest_phone; ?></a>
                                    </strong>
                                </li>

                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"
                                    class="fa fa-times">
                                بستن
                            </button>
                            <!-- <form action="<?php /*echo base_url("home/like/$item->id"); */?>" method="post" data-async
                                  data-target="#like">
                                <button type="submit" class="btn btn-default">Like <span class="badge" id="like">0</span></button>
                            </form>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php $this->load->view("profile_footer"); ?>
<?php $this->load->view("profile_end"); ?>