<?php
/**
 * Created by IntelliJ IDEA.
 * User: ssalehi
 * Date: 6/13/2016
 * Time: 10:57 AM
 */
?>

<!-- jQuery -->
<script src="<?php echo base_url("res/js/jquery.js") ?>"></script>

<!--Google Map-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBX6qypvJiGYYSt5pj4daf_gSYMtWM5sAg"> </script>
<script src="<?php echo base_url('res/js/map.js'); ?>"></script>


<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('res/js/bootstrap.min.js') ?>"></script>

<!-- Custom Theme JavaScript -->
<script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });

    $('#popoverData').popover({ trigger: "hover" });

//    show modal in profile
    $(".portfolio-link").on("click", function () {
        $(this).attr('id').modal('show');
    });
</script>

</body>

</html>

