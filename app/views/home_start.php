<?php
/**
 * Created by IntelliJ IDEA.
 * User: ssalehi
 * Date: 6/13/2016
 * Time: 10:37 AM
 */
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="foodrate is the Iranian Restaurant Ranking, سایت فودریت برای رتبه بندی رستوران های ایران ، بهترین رستوران های ایران را با ما تجربه کنید">
    <meta name="keywords" content="Restaurant, food, yummy, fastfood, iran restaurant, coffee, hot chocolate, foodrate, food ranking">
    <meta name = "keywords" content="فود، غذا، رستوران، کافه، صبحانه، نهار، شام، بوفه، خوشمزه، فودریت، بستنی، شکلات، فسنجون، کباب، تازه، بهترین رستوران">
    <meta name="author" content="">

    <title >foodrate فودریت سایت رتبه بندی رستوران های ایرانی</title>

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('res/img/logo32.png')?>"/>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('res/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('res/css/agency.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('res/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">

    <!-- circle ProgressBar -->
    <link rel="stylesheet" href="<?php echo base_url('res/css/circle.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('res/css/fileinput.min.css'); ?>" >
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css">

    <?php
    if (isset($csss))
    {
    foreach($csss as $css) {
        echo '<link rel="stylesheet" href="' . base_url('res/css/' . $css) . '.css">';
    }
    } ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="index" id="page-top">

