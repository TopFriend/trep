<?php
/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 9/19/2016
 * Time: 2:44 PM
 */
?>
<div id="edit_pass" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close pull-left" type="button" data-dismiss="modal">&times;</button>
                <div class="modal-title text-right persian">تغییر رمز کاربری</div>
            </div>
            <div class="modal-body">
                <form role="form" action="<?php echo base_url('home/rest_register') ?>" method="post"
                      class="registration-form" data-async data-target='#message_alert'>
                    <fieldset>
                        <div class="form-bottom">
                            <div class="input-group form-group">
                                <label class="sr-only" for="pre_pass">عبور قبلی کلمه</label>
                                <input type="password" name="pre_pass" placeholder="کلمه عبور قبلی"
                                       class="form-control text-right" id="pre_pass" validate>
                                <span class="input-group-addon"><i class="fa fa-envelope"></i> </span>
                            </div>
                            <div class="input-group form-group">
                                <label class="sr-only" for="new_pass">کلمه عبور</label>
                                <input type="password" name="new_password" placeholder="کلمه عبور"
                                       class=" form-control text-right" id="new_pass" validate>
                                <span class="input-group-addon"><i class="fa fa-key"></i> </span>
                            </div>
                            <div class="input-group form-group">
                                <label class="sr-only" for="new_pass_repeat">تکرا کلمه عبور</label>
                                <input type="password" name="new_pass_repeat" placeholder="تکرار کلمه عبور"
                                       class=" form-control text-right " id="new_pass_repeat" validate>
                                <span class="input-group-addon"><i class="fa fa-key"></i> </span>
                            </div>
                            <div id="alert"></div>
                            <button type="submit" class="btn btn-next btn-info">تغییر</button>
                        </div>
                    </fieldset>
                </form>
                <div id="message_alert">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#edit_pass').modal();
        $('.registration-form fieldset:first-child').fadeIn('slow');
    });
</script>