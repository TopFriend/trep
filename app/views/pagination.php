<?php if ($size / $page_size > 1) { ?>
    <div class="clearfix"></div>
    <nav aria-label="..." class="text-center">
        <ul class="pagination">
            <li class="page-item <?php if ($current ==1) echo 'disabled' ?>">
      <span class="page-link" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        <span class="sr-only">Previous</span>
      </span>
            </li>
            <?php for ($i = 1; $i < ($size / $page_size) + 1; $i++) { ?>
                <li class="page-item <?php if ($current == $i) {
                    echo "active";
                } ?>"><span class="page-link" onclick="load_rest_list(<?php echo ($i-1)*$page_size.','.(($i-1)*$page_size+$page_size-1) ?>)"><?php echo $i; ?> <span class="sr-only"></span></span></li>
            <?php } ?>

            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
<?php }
?>