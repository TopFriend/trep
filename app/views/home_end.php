<?php
/**
 * Created by IntelliJ IDEA.
 * User: ssalehi
 * Date: 6/13/2016
 * Time: 10:41 AM
 */
?>
<!-- jQuery -->
<script type="text/javascript">
    var base_url = "<?php echo base_url();?>";
</script>
<script src="<?php echo base_url('res/js/jquery-1.11.1.min.js')?>"></script>
<script src="<?php echo base_url('res/js/fileinput.min.js') ?>" ></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('res/js/bootstrap.min.js')?>"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js"></script>

<script src="<?php echo base_url('res/js/agency.min.js') ?>"></script>
<?php foreach($jss as $js)
echo '<script src ="'. base_url('res/js/'.$js.".js").'"></script>'
?>

</body>

</html>
