<?php
/**
 * Created by IntelliJ IDEA.
 * User: ssalehi
 * Date: 6/13/2016
 * Time: 11:02 AM
 */
?>


<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <h4><strong>www.foodrate.ir</strong>
                </h4>
                <p>تهران، ستارخان خیابان پاتریس</p>
                <ul class="list-unstyled">
                    <li><i class="fa fa-phone fa-fw"></i> (98)21-887890</li>
                    <li><i class="fa fa-envelope-o fa-fw"></i>  <a href="mailto:name@example.com">tacsaman@yahoo.com</a>
                    </li>
                </ul>
                <br>
                <ul class="list-inline">
                    <li>
                        <a href="http://instagram.com/foodrate.ir"><i class="fa fa-instagram fa-fw fa-3x"></i></a>
                    </li>
                    <li>
                        <a href="http://telegram.me/foodrate.ir"><i class="fa fa-phone-square fa-fw fa-3x"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
                    </li>
                </ul>
                <hr class="small">
                <p class="text-muted">Copyright &copy; www.foodrate.ir 2016</p>
            </div>
        </div>
    </div>
</footer>
