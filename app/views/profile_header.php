<?php
/**
 * Created by IntelliJ IDEA.
 * User: ssalehi
 * Date: 6/13/2016
 * Time: 11:01 AM
 */
?>
<!-- Navigation -->
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
        <li class="sidebar-brand">
            <a class="text-center" href="#top"  onclick = $("#menu-close").click(); ><?php echo $profile_info->rest_name?></a>
        </li>
        <li>
            <a class="text-center" href="#top" onclick = $("#menu-close").click(); >ابتدای صفحه</a>
        </li>
        <li>
            <a class="text-center" href="#about" onclick = $("#menu-close").click(); >درباره</a>
        </li>
        <li>
            <a class="text-center" href="#services" onclick = $("#menu-close").click(); >سرویس ها</a>
        </li>
        <li>
            <a class="text-center" href="#portfolio" onclick = $("#menu-close").click(); >گالری</a>
        </li>
        <li>
            <a class="text-center" href="#contact" onclick = $("#menu-close").click(); >تماس و رزرو</a>
        </li>
    </ul>
</nav>
<!-- Header -->
<header id="top" class="header" style="background: rgba(0, 0, 0, 0) url('<?php echo media_path($profile_info->rest_email,$profile_info->rest_name);  ?>') no-repeat scroll center center / cover ;">
    <div class="text-vertical-center">
        <h1 class="persian-titr"><?php echo $profile_info->rest_name?></h1>
        <h3 class="persian">بزودی با معرفی بهترین های تهران و شهرستان ها</h3>
        <br>
        <a href="#about" class="btn btn-dark btn-lg">همین حالا ثبت کنید</a>
    </div>
</header>

