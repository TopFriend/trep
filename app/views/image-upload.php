<form enctype="multipart/form-data">
    <div class="form-group">
        <input id="input-700" type="file" multiple class="file-loading">
    </div>
</form>
<script>
    $(document).ready(function(){
        $('#next_page').on('click', function () {
            $("#input-700").fileinput({
                uploadUrl: "<?php echo base_url('home/upload_image') ?>", // server upload action
                uploadAsync: true,
                maxFileCount: 2,
                validateInitialCount: true,
                uploadExtraData: {
                    dir_key: $('#rest_email').val(),
                    rest_name:$('#rest_name').val()
                }
            });
        })
    });

</script>