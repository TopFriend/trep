<div class="col-md-4 col-sm-6 portfolio-item">
    <a href="<?php echo base_url('profile/'.$id); ?>" class="portfolio-link" data-toggle="modal">
        <div class="portfolio-hover">
            <div class="portfolio-hover-content">
                <i class="fa-3x persian-titr">1234</i>
            </div>
        </div>
        <img src="<?php echo $img_url; ?>" class="img-responsive" alt="<?php echo $rest_name; ?>">
    </a>

    <div class="portfolio-caption">
        <h3><?php echo $rest_name; ?></h3>
        <p><?php echo $rest_address;?> </p>
    </div>
    <?php if (true/*$this->session->logedin*/) { ?>
        <div class="portfolio-caption">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="service-item">
                                <span class="fa-stack fa-2x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-map-marker fa-stack-1x text-primary"></i>
                            </span>
                        <h4>
                            <strong>مکان</strong>
                        </h4>
                        <p>موقعیت رستوران مناسب است؟
                        </p>
                        <a id="popoverData" href="#" class="btn"  data-content="Popover with data-trigger"
                           rel="popover" data-placement="bottom" data-original-title="Title">
                            <div class="c100 p60 small green" align="center">
                                <span>60%</span>

                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="service-item">
                                <span class="fa-stack fa-2x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-cutlery fa-stack-1x text-primary"></i>
                            </span>
                        <h4>
                            <strong>کیفیت</strong>
                        </h4>

                        <p>از کیفیت سرویس ها راضی هستید؟
                        </p>
                        <a href="#" class="btn">
                            <div class="c100 p60 small green" align="center">
                                <span>60%</span>

                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="service-item">
                                <span class="fa-stack fa-2x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-credit-card fa-stack-1x text-primary"></i>
                            </span>
                        <h4>
                            <strong>هزینه</strong>
                        </h4>

                        <p>هزینه سرویس ها مناسب است؟</p>
                        <a href="#" class="btn">
                            <div class="c100 p40 small green" align="center">
                                <span>40%</span>
                                <div class="slice">
                                    <div class="bar"></div>
                                    <div class="fill"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

        </div>
    <?php } ?>
</div>
