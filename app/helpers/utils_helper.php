<?php

/**
 * Created by IntelliJ IDEA.
 * User: saman
 * Date: 10/1/2016
 * Time: 11:16 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

     function __init()
    {
        $ci = & get_instance();
        $ci->load->library('email');
        $ci->load->helper('string');
        return $ci;
    }

     function send_mail($from,$to,$subject,$message)
    {
        $ci =  __init();
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'mail.foodrate.ir';
        $config['smtp_port']    = '26';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'info@foodrate.ir';
        $config['smtp_pass']    = 'Sami123654';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not
        $ci->email->initialize($config);
        $ci->email->from($from,"foodrate.ir");
        $ci->email->to($to);
        $ci->email->subject($subject);
        $ci->email->message($message);
        $ci->email->send();
    }

     function create_rand_pass()
    {
        __init();
        return random_string('alnum',8);
    }

    function media_path($rest_email,$rest_name)
    {
        $email =str_replace("@","-",str_replace(".","-",$rest_email));   //remove @ ,.
        return base_url("/up/".$email."/".str_replace(" ","_",$rest_name).".jpg");
    }